class profile::antivirus (
  Array[String] $whitelist = [],
  Array[String] $scan_paths = ['/'],
  String $exclude_regex = '^(/sys|/dev|/proc|/run|/var/lib/lxc|/var/lib/lxcfs|/var/lib/docker|/var/lib/docker/overlay2|/var/lib/containerd|/var/cache/lxc)',
  Optional[String] $time = undef,
  Optional[String] $mail_notification = undef,
) {

  class { 'clamav':
    manage_clamd     => true,
    manage_freshclam => true,
    clamd_options    => {
      'ExcludePath'      => $exclude_regex,
      'CrossFilesystems' => false,
    }
  }

  $whitelist_template = @(END)
# This file is managed by Puppet
# MD5:SIZE:ID_NAME
# generated with sigtool --md5 PATH
<% @whitelist.each do |entry| -%>
<%= entry %>
<% end -%>
  END

  file { '/var/lib/clamav/whitelist.fp':
    ensure  => present,
    content => inline_template($whitelist_template),
    owner   => 'clamav',
    group   => 'clamav',
    mode    => '0700',
  }

  file { '/usr/local/sbin/av-scan.sh':
    ensure  => present,
    content => template('profile/av-scan.sh.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0700',
  }

  file { '/etc/systemd/system/av-scan.service':
    ensure  => present,
    content => template('profile/av-scan.service.erb'),
  }

  if $time != undef {
      # Timer (aka cron job)
      file { '/etc/systemd/system/av-scan.timer':
        ensure    => present,
        content   => template('profile/av-scan.timer.erb'),
        subscribe => File['/etc/systemd/system/av-scan.service'],
        notify    => Service['av-scan'],
      }
      # TODO: systemctl daemon-reload when service files change
      service { 'av-scan':
        ensure   => running,
        enable   => true,
        provider => 'systemd',
        name     => 'av-scan.timer',
        require  => [
          File['/etc/systemd/system/av-scan.service'],
          File['/etc/systemd/system/av-scan.timer'],
          File['/usr/local/sbin/av-scan.sh'],
        ],
      }
  }
}
