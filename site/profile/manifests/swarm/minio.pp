class profile::swarm::minio (
  String $secret_key,
  String $access_key,
  String $console_salt,
  String $console_secret,
  String $browser = 'on',
  String $version = 'latest',
  String $console_version = 'latest',
) {

file { [
        '/mnt/data/minio',
        '/mnt/data/minio/data',
        '/mnt/data/minio/conf',
        '/mnt/data/minio/temp',
    ]:
    ensure => directory,
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'minio' => {
        'image'       => "minio/minio:${version}",
        'command'     => 'server /data',
        'read_only'   => true,
        'volumes'     => [
          '/mnt/data/minio/data:/data',
          '/mnt/data/minio/conf:/root/.minio',
          '/mnt/data/minio/temp:/tmp',
        ],
        'deploy'      => {
          'labels' => [
            'traefik.enable=true',
            'traefik.http.routers.minio.tls.certresolver=le',
            'traefik.http.services.minio.loadbalancer.server.port=9000',
            'traefik.http.routers.minio.rule=Host(`s3.cubieserver.de`)',
            'traefik.http.routers.minio.middlewares=security-headers@file',
          ],
          'resources' => {
            'limits' => {
              'memory' => '512M',
              'cpus'   => '1.0',
            },
            'reservations' => {
              'memory' => '256M',
            }
          }
        },
        'environment' => [
          "MINIO_BROWSER=${browser}",
          "MINIO_SECRET_KEY=${secret_key}",
          "MINIO_ACCESS_KEY=${access_key}",
        ],
      },
      'minio_console' => {
        'image'       => "minio/console:${console_version}",
        'read_only'   => false,
        'command'     => 'server',
        'environment' => [
          "CONSOLE_PBKDF_PASSPHRASE=${console_secret}",
          "CONSOLE_PBKDF_SALT=${console_salt}",
          'CONSOLE_MINIO_SERVER=http://minio:9000',
          'CONSOLE_PROMETHEUS_SERVER=http://prometheus:9090',
        ],
        'deploy'      => {
          'labels' => [
            'traefik.enable=true',
            'traefik.http.routers.minio_console.tls.certresolver=le',
            'traefik.http.services.minio_console.loadbalancer.server.port=9090',
            'traefik.http.routers.minio_console.rule=Host(`s3console.cubieserver.de`)',
            'traefik.http.routers.minio_console.middlewares=security-headers@file',
          ],
          'resources' => {
            'limits' => {
              'memory' => '128M',
              'cpus'   => '0.25',
            },
            'reservations' => {
              'memory' => '64M',
            }
          }
        },
      }
    }
  }

  profile::to_compose { 'minio':
    compose => $compose,
  }

}
