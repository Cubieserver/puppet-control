class profile::swarm::cockroach (

  String $version = 'latest',
  String $cluster_name = 'cubie-cluster',

) {

  file { '/var/lib/cockroach':
    ensure => directory,
  }
  file { '/var/lib/cockroach/roach-1':
    ensure => directory,
  }
  file { '/var/backups/cockroachdb':
    ensure => directory,
  }
  file { '/usr/local/bin/cockroach-backups.sh':
    ensure  => present,
    mode    => '0751',
    content => template('profile/cockroach-backups.sh.erb'),
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'cockroach' => {
        'image'   => "cockroachdb/cockroach:${version}",
        'volumes' => [
          '/var/lib/cockroach/roach-1:/cockroach/cockroach-data',
        ],
        'command' => "start-single-node --cluster-name=${cluster_name} --logtostderr=WARNING --log-file-verbosity=WARNING --insecure",
        'deploy' => {
          'labels' => [
            'traefik.enable=true',
            'traefik.http.routers.cockroach.tls.certresolver=le',
            'traefik.http.services.cockroach.loadbalancer.server.port=8080',
            'traefik.http.routers.cockroach.rule=Host(`infra.cubieserver.de`) && PathPrefix(`/cockroach/`)',
            'traefik.http.routers.cockroach.middlewares=admin-auth@file,security-headers@file,strip-cockroach',
            'traefik.http.middlewares.strip-cockroach.stripprefix.prefixes=/cockroach',
          ],
          'resources' => {
            'limits' => {
              'memory' => '2048M',
              'cpus'   => '2.0',
            },
            'reservations' => {
              'memory' => '512M',
            }
          }
        },
      },
      'cockroach_backups' => {
        'image'       => "cockroachdb/cockroach:${version}",
        'volumes'     => [
          '/var/backups/cockroachdb:/backups',
          '/usr/local/bin/cockroach-backups.sh:/cockroach-backups.sh:ro',
        ],
        'entrypoint'  => '/bin/bash',
        'command'     => [
          '/cockroach-backups.sh',
          'cockroach',
        ],
        'environment' => [
          'ROTATE_DAYS=7',
          'BACKUP_INTERVAL=24h',
        ],
        'deploy' => {
          'resources' => {
            'limits' => {
              'memory' => '128M',
              'cpus'   => '0.25',
            },
            'reservations' => {
              'memory' => '32M',
            }
          }
        }
      }
    }
  }

  profile::to_compose { 'cockroach':
    compose => $compose,
  }

}
