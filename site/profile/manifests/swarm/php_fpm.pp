class profile::swarm::php_fpm (
) {

  # TODO: update image
  $compose = {
    'version' => '3.7',
    'services' => {
      'php-fpm' => {
        'image'   => 'jacksgt/php:fpm',
        'volumes' => [
          '/var/www:/var/www',
        ],
        'deploy' => {
          'resources' => {
            'limits' => {
              'memory' => '128M',
              'cpus'   => '0.5',
            },
            'reservations' => {
              'memory' => '64M',
            }
          }
        }
      }
    }
  }

  profile::to_compose { 'php-fpm':
    compose => $compose,
  }

}
