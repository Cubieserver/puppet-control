class profile::swarm::swarmprom (

  String $minio_auth_bearer,
  String $grafana_admin_user = 'admin',
  String $prometheus_retention = '30d',
  String $docker_gwbridge_ip = '172.18.0.1',

  ) {

  file { '/var/lib/prometheus':
    ensure => directory,
    owner  => nobody,
  }

  # config directories and files
  file { '/etc/swarmprom/':
    ensure => directory,
  }
  file { '/etc/swarmprom/dockerd-exporter.Caddyfile':
    ensure  => present,
    content => template('profile/swarmprom/dockerd-exporter.Caddyfile.erb'),
    require => File['/etc/swarmprom'],
  }

  # Prometheus configuration
  file { '/etc/swarmprom/prometheus':
    ensure  => directory,
    require => File['/etc/swarmprom'],
  }
  file { '/etc/swarmprom/prometheus/swarm_node.rules.yml':
    ensure  => present,
    content => template('profile/swarmprom/swarm_node.rules.yml'),
    require => File['/etc/swarmprom/prometheus'],
  }
  file { '/etc/swarmprom/prometheus/swarm_task.rules.yml':
    ensure  => present,
    content => template('profile/swarmprom/swarm_task.rules.yml'),
    require => File['/etc/swarmprom/prometheus'],
  }
  file { '/etc/swarmprom/prometheus/cockroach-aggregation.rules.yml':
    ensure  => present,
    content => template('profile/swarmprom/cockroach-aggregation.rules.yml'),
    require => File['/etc/swarmprom/prometheus'],
  }
  file { '/etc/swarmprom/prometheus/prometheus.yml':
    ensure  => present,
    content => template('profile/swarmprom/prometheus.yml'),
    require => File['/etc/swarmprom/prometheus'],
  }
  # maybe should just use ensure directory contents (?)


  # simulates the following:
  # profile::to_compose { 'swarmprom':
  #   compose => $compose,
  # }
  file { '/mnt/data/swarm/swarmprom.yaml':
    ensure  => present,
    content => template('profile/swarmprom/docker-services.yml.erb')
  }

}
