class profile::swarm::redis (
  String $version = 'latest'
) {

  # TODO: update image
  $compose = {
    'version' => '3.7',
    'services' => {
      'redis' => {
        'image'   => "redis:${version}",
        'command' => 'redis-server --save "" --appendonly no',
        'deploy' => {
          'resources' => {
            'limits' => {
              'memory' => '128M',
              'cpus'   => '0.5',
            },
            'reservations' => {
              'memory' => '32M',
            }
          }
        }
      }
    }
  }

  profile::to_compose { 'redis':
    compose => $compose,
  }

}
