class profile::swarm::ejabberd (
  String $certfile,
  Array[String] $authmethods,
  Array[String] $hosts = ['localhost'],
  Optional[String] $ldap_host = undef,
  Optional[String] $ldap_base = undef,
  Optional[String] $ldap_bind = undef,
  Optional[String] $ldap_password = undef,
  Optional[String] $ldap_encrypt = undef,
  Optional[Integer] $ldap_port = undef,
  Optional[String] $stun_server = undef,
  Optional[String] $turn_server = undef,
  Optional[String] $turn_secret = undef,
  Integer $log_level = 5,
  String $node_name = 'ejabberd@localhost',
  String $version = 'latest',
  ) {

  # TODO: use SQL database instead of mnesia

  user { 'ejabberd':
    ensure => present,
    # user id inside docker container "ejabberd/ecs"
    uid    => 9000,
    groups => ['ssl-cert'],
    shell  => '/bin/sh',
  }

  # TODO: stricter permissions
  file { ['/etc/ejabberd/', '/mnt/data/ejabberd', '/mnt/data/ejabberd/upload', '/mnt/data/ejabberd/database']:
    ensure  => directory,
    owner   => ejabberd,
    group   => ejabberd,
    require => User['ejabberd'],
  }

  cron { 'copy cubieserver.de key for ejabberd':
    ensure  => 'present',
    command => '/bin/cat /etc/ssl/private/cubieserver.de.key /etc/ssl/private/cubieserver.de.crt > /etc/ssl/private/ejabberd.pem',
    hour    => 22,
    minute  => 24,
    weekday => 'Tuesday',
    # run once per week
    target  => 'root',
    user    => 'root',
  }

  file { '/etc/ssl/private/ejabberd.pem':
    ensure  => present,
    owner   => ejabberd,
    mode    => '0600',
    require => User['ejabberd'],
  }

  file { '/etc/ejabberd/ejabberd.yml':
    ensure  => present,
    owner   => 'ejabberd',
    mode    => '0640',
    content => template('profile/ejabberd.yml.erb'),
    require => File['/etc/ejabberd/'],
  }

  file { '/etc/ejabberd/ejabberdctl.cfg':
    ensure  => present,
    mode    => '0644',
    content => template('profile/ejabberdctl.cfg.erb'),
    require => File['/etc/ejabberd/'],
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'ejabberd' => {
        'image' => "ejabberd/ecs:${version}",
        'volumes' => [
#          '/var/run/mysqld/mysqld.sock:/var/run/mysqld/mysqld.sock',
          '/mnt/data/ejabberd/upload:/home/ejabberd/upload',
          '/mnt/data/ejabberd/database:/home/ejabberd/database',
          '/etc/ejabberd/ejabberd.yml:/home/ejabberd/conf/ejabberd.yml', # TODO: ro?
          '/etc/ejabberd/ejabberdctl.cfg:/home/ejabberd/conf/ejabberdctl.cfg', # TODO: ro?
          '/etc/ssl/private/ejabberd.pem:/home/ejabberd/conf/ejabberd.pem:ro',
        ],
        'ports' => [
          '5222:5222',
          '5269:5269',
          '5443:5443',
        ],
        'environment' => [
          'ERLANG_NODE=ejabberd@localhost',
        ],
        'deploy' => {
          'resources' => {
            'limits' => {
              'memory' => '512M',
              'cpus'   => '0.5',
            },
            'reservations' => {
              'memory' => '256M',
            }
          }
        }
      }
    }
  }

  profile::to_compose { 'ejabberd':
    compose => $compose,
  }

}
