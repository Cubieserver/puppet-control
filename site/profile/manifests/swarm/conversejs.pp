class profile::swarm::conversejs (
#  String $websocket_url,
  String $bosh_service_url,
  String $version = 'latest',
) {

  $compose = {
    'version' => '3.7',
    'services' => {
      'conversejs' => {
        'image'   => "sailors/conversejs:${version}",
        'deploy' => {
          'labels' => [
            'traefik.port=8080',
            'traefik.frontend.rule=Host:chat.cubieserver.de',
            'traefik.enable=true',
            'traefik.frontend.passHostHeader=true',
          ],
        },
        'environment' => [
#          "WEBSOCKET_URL=${websocket_url}",
          "BOSH_SERVICE_URL=${bosh_service_url}",
        ],
      }
    }
  }

  profile::to_compose { 'conversejs':
    compose => $compose,
  }

}
