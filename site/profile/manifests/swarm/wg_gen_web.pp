class profile::swarm::wg_gen_web (

  String $admin_basic_auth_password,
  String $wireguard_interface = 'wg0',
  String $wireguard_conf_dir = '/etc/wireguard/',
  String $version = 'latest',

) {

  $compose = {
    'version' => '3.7',
    'services' => {
      'wg_gen_web' => {
        'image'   => "vx3r/wg-gen-web:${version}",
        'volumes' => [
          "${wireguard_conf_dir}:/data"
        ],
        'deploy' => {
          'placement' => {
            constraints => [
              'node.role == manager',
            ],
          },
          'labels' => [
            'traefik.port=8080',
            'traefik.frontend.rule=Host:vpn.cubieserver.de',
            'traefik.enable=true',
            "traefik.frontend.auth.basic.users=admin:${admin_basic_auth_password}",
          ],
        },
        'environment' => [
          'WG_CONF_DIR=/data',
          "WG_INTERFACE_NAME=${wireguard_interface}.conf",
          'OAUTH2_PROVIDER_NAME=fake',
        ],
      }
    }
  }

  profile::to_compose { 'wg_gen_web':
    compose => $compose,
  }

  file { '/etc/systemd/system/wg-gen-web.service':
    ensure  => present,
    content => template('profile/wg-gen-web.service.erb'),
    notify  => Exec['systemd daemon-reload for wg-gen-web'],
  }

  file { '/etc/systemd/system/wg-gen-web.path':
    ensure  => present,
    content => template('profile/wg-gen-web.path.erb'),
    notify  => Exec['systemd daemon-reload for wg-gen-web'],
  }

  exec { 'systemd daemon-reload for wg-gen-web':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
  }

  service { 'wg-gen-web.path':
    ensure   => running,
    enable   => true,
    require  => File['/etc/systemd/system/wg-gen-web.service'],
    provider => 'systemd',
  }

}
