class profile::swarm::node_exporter (

  String $version = 'latest',

) {

  file { '/var/lib/node-exporter':
    ensure => directory,
    owner  => 'root',
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'node-exporter' => {
        'image'       => "quay.io/prometheus/node-exporter:${version}",
        'read_only'   => true,
        'volumes'     => [
          '/var/lib/node-exporter:/var/lib/node-exporter',
          '/:/host:ro,rslave',
        ],
        'environment' => [
          'NODE_ID={{.Node.ID}}'
        ],
        'command'     => [
          '--path.rootfs=/host',
          '--collector.disable-defaults',
          '--web.disable-exporter-metrics',
          '--collector.cpu',
          '--collector.cpufreq',
          '--collector.diskstats',
          '--collector.edac',
          '--collector.entropy',
          '--collector.filefd',
          '--collector.filesystem',
          '--collector.filesystem.ignored-mount-points="^/(dev|proc|run|sys|mnt|media|var/lib/docker/.+)($$|/)"',
          '--collector.hwmon',
          '--collector.loadavg',
          '--collector.mdadm',
          '--collector.meminfo',
          '--collector.netclass',
          '--collector.netdev',
          '--collector.netstat',
          '--collector.powersupplyclass',
          '--collector.rapl',
          '--collector.textfile',
          '--collector.textfile.directory=/var/lib/node-exporter',
          '--collector.schedstat',
          '--collector.sockstat',
          '--collector.stat',
          # TODO: add and configure systemd collector
          # '--collector.systemd',
          '--collector.thermal_zone',
          '--collector.time',
          '--collector.uname',
          '--collector.vmstat',
        ],
        'deploy'      => {
          'mode'      => 'global',
          'resources' => {
            'limits' => {
              'memory' => '128M',
              'cpus'   => '0.5',
            },
            'reservations' => {
              'memory' => '64M',
            }
          }
        },
      }
    }
  }

  profile::to_compose { 'node-exporter':
    compose => $compose,
  }

}
