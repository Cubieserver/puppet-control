class profile::swarm::drone (

  String $gitea_server,
  String $gitea_client_id,
  String $gitea_client_secret,
  String $rpc_secret,
  String $version = 'latest',

) {

  file { '/mnt/data/drone':
    ensure => directory,
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'drone' => {
        'image'       => "drone/drone:${version}",
        'read_only'   => true,
        'volumes'     => [
          '/var/run/docker.sock:/var/run/docker.sock',
          '/mnt/data/drone:/data',
        ],
        'deploy'      => {
          'labels' => [
            'traefik.enable=true',
            'traefik.http.routers.drone.tls.certresolver=le',
            'traefik.http.services.drone.loadbalancer.server.port=80',
            'traefik.http.routers.drone.rule=Host(`drone.cubieserver.de`)',
            'traefik.http.routers.drone.middlewares=security-headers@file',
          ],
          'resources' => {
            'limits' => {
              'memory' => '128M',
              'cpus'   => '0.5',
            },
            'reservations' => {
              'memory' => '64M',
            }
          }
        },
        'environment' => [
          "DRONE_GITEA_SERVER=${gitea_server}",
          "DRONE_GITEA_CLIENT_ID=${gitea_client_id}",
          "DRONE_GITEA_CLIENT_SECRET=${gitea_client_secret}",
          'DRONE_SERVER_PROTO=https',
          'DRONE_SERVER_HOST=drone.cubieserver.de',
          "DRONE_RPC_SECRET=${rpc_secret}",
        ],
      }
    }
  }

  profile::to_compose { 'drone':
    compose => $compose,
  }

}
