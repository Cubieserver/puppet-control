class profile::swarm::u9k (

  String $base_url,
  String $s3_endpoint,
  String $s3_region,
  String $s3_bucket,
  String $s3_access_key,
  String $s3_secret_key,
  String $smtp_hostport,
  String $smtp_user,
  String $smtp_password,
  String $db_conn_url = 'postgresql://u9k@cockroach:26257/u9k?sslmode=disable',
  String $version = 'latest',

) {

  $compose = {
    'version' => '3.7',
    'services' => {
      'u9k' => {
        'image'       => "jacksgt/u9k:${version}",
        'user'        => '0', # docker swarm cannot set proper permissions for tmpfs mount
        'read_only'   => true,
        'volumes'     => [
          {
            'type'   => 'tmpfs',
            'target' => '/tmp',
            'tmpfs'  => {
              'size' => 1000000000, # 1 GB
            },
          },
        ],
        'deploy'      => {
          'labels' => [
            'traefik.enable=true',
            'traefik.http.routers.u9k.tls.certresolver=le',
            'traefik.http.services.u9k.loadbalancer.server.port=3000',
            'traefik.http.routers.u9k.rule=Host(`u9k.de`)',
            'traefik.http.routers.u9k.middlewares=security-headers@file',
          ],
        },
        'environment' => [
          "U9K_BASE_URL=${base_url}",
          "U9K_DB_CONN_URL=${db_conn_url}",
          "U9K_S3_ENDPOINT=${s3_endpoint}",
          "U9K_S3_REGION=${s3_region}",
          "U9K_S3_BUCKET=${s3_bucket}",
          "U9K_S3_ACCESS_KEY=${s3_access_key}",
          "U9K_S3_SECRET_KEY=${s3_secret_key}",
          "U9K_SMTP_HOSTPORT=${smtp_hostport}",
          "U9K_SMTP_USER=${smtp_user}",
          "U9K_SMTP_PASSWORD=${smtp_password}",
          'U9K_LISTEN_ADDR=0.0.0.0',
          'U9K_PORT=3000',
        ],
      }
    }
  }

  profile::to_compose { 'u9k':
    compose => $compose,
  }

}
