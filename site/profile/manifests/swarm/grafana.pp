class profile::swarm::grafana (

  String $admin_password,
  String $admin_name = 'admin',
  String $version = 'latest',
  String $log_level = 'warn',

) {

  file { '/mnt/data/grafana':
    ensure => directory,
    owner  => '472',
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'grafana' => {
        'image'       => "grafana/grafana:${version}",
        'read_only'   => true,
        'volumes'     => [
          '/mnt/data/grafana:/var/lib/grafana',
        ],
        'deploy'      => {
          'labels' => [
            'traefik.enable=true',
            'traefik.http.routers.grafana.tls.certresolver=le',
            'traefik.http.services.grafana.loadbalancer.server.port=3000',
            'traefik.http.routers.grafana.rule=Host(`infra.cubieserver.de`) && PathPrefix(`/grafana/`)',
            'traefik.http.middlewares.strip-grafana.stripprefix.prefixes=/grafana',
            'traefik.http.routers.grafana.middlewares=admin-auth@file,security-headers@file,strip-grafana',
          ],
          'resources' => {
            'limits' => {
              'memory' => '128M',
              'cpus'   => '0.5',
            },
            'reservations' => {
              'memory' => '64M',
            }
          }
        },
        'environment' => [
          'GF_SERVER_ROOT_URL=https://infra.cubieserver.de/grafana/',
          "GF_LOG_LEVEL=${log_level}",
          'GF_ANALYTICS_CHECK_FOR_UPDATES=false',
          'GF_SECURITY_DISABLE_GRAVATAR=true',
          'GF_SECURITY_COOKIE_SECURE=true',
          'GF_SECURITY_DISABLE_INITIAL_ADMIN_CREATION=true',
          "GF_SECURITY_ADMIN_USER=${admin_name}",
          "GF_SECURITY_ADMIN_PASSWORD=${admin_password}",
        ]
      }
    }
  }

  profile::to_compose { 'grafana':
    compose => $compose,
  }

}
