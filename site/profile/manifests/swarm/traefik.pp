class profile::swarm::traefik (
  String $admin_basic_auth_password,
  String $version = 'latest',
  String $log_level = 'INFO',
) {

  file { '/etc/traefik':
    ensure => directory,
    mode   => '0700',
  }

  file { '/etc/traefik/traefik.toml':
    ensure  => present,
    content => template('profile/traefik.toml.erb'),
    require => File['/etc/traefik'],
  }

  file { '/etc/traefik/acmev2.json':
    ensure  => present,
    mode    => '0600',
    require => File['/etc/traefik'],
  }

  file { '/etc/traefik/dynamic':
    ensure  => directory,
    require => File['/etc/traefik'],
  }

  file { '/etc/traefik/dynamic/conf.toml':
    ensure  => present,
    content => template('profile/traefik-dynamic.toml.erb'),
    require => File['/etc/traefik/dynamic'],
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'traefik' => {
        'image'           => "traefik:${version}",
        'read_only'       => true,
        'volumes'         => [
          '/var/run/docker.sock:/var/run/docker.sock',
          '/etc/traefik/acmev2.json:/etc/traefik/acmev2.json',
          '/etc/traefik/traefik.toml:/etc/traefik/traefik.toml:ro',
          '/etc/traefik/dynamic/:/etc/traefik/dynamic/:ro',
        ],
        'deploy'          => {
          'placement'     => {
            'constraints' => [ 'node.role == manager' ]
          },
          'labels'        => [
            'traefik.enable=true',
            'traefik.http.routers.traefik.tls.certresolver=le',
            'traefik.http.routers.traefik.service=api@internal',
            'traefik.http.routers.traefik.rule=Host(`infra.cubieserver.de`) && (PathPrefix(`/api`) || PathPrefix(`/dashboard`))',
            'traefik.http.routers.traefik.middlewares=admin-auth@file,security-headers@file',
            # Dummy service for Swarm port detection. The port can be any valid integer value.
            'traefik.http.services.dummy-svc.loadbalancer.server.port=9999',
          ],
          'resources' => {
            'limits' => {
              'memory' => '256M',
              'cpus'   => '2.0',
            },
            'reservations' => {
              'memory' => '64M',
            }
          }
        },
        'ports'           => [
          {
            'target'    => 80,
            'published' => 80,
            'protocol'  => 'tcp',
            'mode'      => 'host',
          },
          {
            'target'    => 443,
            'published' => 443,
            'protocol'  => 'tcp',
            'mode'      => 'host',
          },
        ],
      }
    }
  }

  profile::to_compose { 'traefik':
    compose => $compose,
  }

  # manual firewall configuration because the ports are published in host mode
  firewall { '100 allow HTTP traffic':
    dport  => 80,
    proto  => tcp,
    action => accept,
  }

  firewall { '100 allow HTTP traffic (v6)':
    dport    => 80,
    proto    => tcp,
    action   => accept,
    provider => 'ip6tables',
  }

  firewall { '100 allow HTTPS traffic':
    dport  => 443,
    proto  => tcp,
    action => accept,
  }

  firewall { '100 allow HTTPS traffic (v6)':
    dport    => 443,
    proto    => tcp,
    action   => accept,
    provider => 'ip6tables',
  }

  # not sure if this belongs here

  # set up cron job to regularly export cubieserver.de key and certificate
  file { '/usr/local/bin/export-traefik-v2-cert-key.sh':
    ensure  => present,
    mode    => '0751',
    content => template('profile/export-traefik-v2-cert-key.sh'),
  }

  package { 'jq':
    ensure => present,
  }

  cron { 'export cubieserver.de key cert from traefik v2':
    ensure  => 'present',
    command => '/usr/local/bin/export-traefik-v2-cert-key.sh cubieserver.de',
    hour    => 22,
    minute  => 22,
    target  => 'root',
    user    => 'root',
    require => [
                File['/usr/local/bin/export-traefik-v2-cert-key.sh'],
                Package['jq'],
    ],
  }

}
