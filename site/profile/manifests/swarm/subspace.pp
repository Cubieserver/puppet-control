class profile::swarm::subspace (
  String $admin_basic_auth_password,
  String $version = 'latest',
  Boolean $debug = true,
) {

  file { '/mnt/data/subspace':
    ensure => directory,
    mode   => '0700',
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'traefik' => {
        'image'           => "subspacecommunity/subspace:${version}",
        'volumes'         => [
          '/mnt/data/subspace:/data',
        ],
        'environment'     => [
          "SUBSPACE_HTTP_HOST=vpn.cubieserver.de",
          "SUBSPACE_HTTP_INSECURE=true",
          "SUBSPACE_LETSENCRYPT=false",
          "SUBSPACE_DEBUG=${debug}",
        ],
        cap_add => [
          'NET_ADMIN',
        ],
        'deploy'          => {
          'placement'     => {
            'constraints' => [ 'node.role == manager' ]
          },
          'labels'        => [
            'traefik.port=80',
            'traefik.frontend.rule=Host:vpn.cubieserver.de',
            'traefik.enable=true',
            "traefik.frontend.auth.basic.users=admin:${admin_basic_auth_password}",
          ],
        },
      }
    }
  }

  profile::to_compose { 'subspace':
    compose => $compose,
  }

  # TODO: move this to wireguard class
  firewall { '100 allow VPN traffic':
    dport  => 51280,
    proto  => tcp,
    action => accept,
  }

  firewall { '100 allow VPN traffic (v6)':
    dport    => 51280,
    proto    => tcp,
    action   => accept,
    provider => 'ip6tables',
  }
}
