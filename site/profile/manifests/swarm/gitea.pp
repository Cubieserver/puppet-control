class profile::swarm::gitea (
  String $db_password,
  String $mail_host,
  String $mail_user,
  String $mail_password,
  String $secret_key,
  String $internal_token,
  String $oauth2_jwt_secret,
  String $log_level = 'Warn',
  String $version = 'latest',
) {

  file { ['/mnt/data/gitea', '/mnt/data/gitea/custom', '/mnt/data/gitea/custom/conf']:
    ensure => directory,
    # uid / gid matches user 'git' inside container
    owner  => 1000,
    group  => 1000,
  }

  ::mysql::db { 'gitea':
    user     => 'gitea',
    password => $db_password,
    host     => 'localhost',
    grant    => ['ALL'],
  }

  file { '/mnt/data/gitea/custom/conf/app.ini':
    ensure  => present,
    content => template('profile/gitea.ini.erb'),
    # uid / gid matches user 'git' inside container
    owner   => 1000,
    group   => 1000,
    require => File['/mnt/data/gitea/custom/conf'],
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'gitea' => {
        'image'           => "gitea/gitea:${version}",
        'volumes'         => [
          '/var/run/mysqld/mysqld.sock:/var/run/mysqld/mysqld.sock',
          '/mnt/data/gitea:/data',
        ],
        'deploy'          => {
          'labels'        => [
            'traefik.enable=true',
            'traefik.http.routers.gitea.tls.certresolver=le',
            'traefik.http.services.gitea.loadbalancer.server.port=3000',
            'traefik.http.routers.gitea.rule=Host(`git.cubieserver.de`)',
            'traefik.http.routers.gitea.middlewares=security-headers@file',
            # need to specify the correct network, because gitea container exposes
            # a port in host mode (22) and thus has multiple networks
            'traefik.docker.network=stack_default',
          ],
          'resources' => {
            'limits' => {
              'memory' => '1024M',
              'cpus'   => '1.0',
            },
            'reservations' => {
              'memory' => '128M',
            }
          }
        },
        ports             => [
          '2222:22',
        ],
        environment       => [
          'GITEA_WORK_DIR=/data',
          'GITEA_CUSTOM=/data/custom',
          'USER=git',
          'HOME=/data/home',
        ],
      }
    }
  }

  profile::to_compose { 'gitea':
    compose => $compose,
  }

}
