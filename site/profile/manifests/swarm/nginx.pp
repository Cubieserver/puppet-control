class profile::swarm::nginx (
  String $version = 'latest',
) {

  file { '/etc/nginx':
    ensure => directory,
  }

  # TODO: maybe use voxpupuli puppet-nginx module for configuration?

  file { '/etc/nginx/nginx.conf':
    ensure  => present,
    content => template('profile/nginx.conf.erb'),
    require => File['/etc/nginx'],
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'nginx' => {
        'image'           => "nginx:${version}",
        'volumes'         => [
          '/etc/nginx/nginx.conf:/etc/nginx/nginx.conf:ro',
          '/var/www:/var/www:ro',
        ],
        'deploy' => {
          'labels' => [
            'traefik.enable=true',
            'traefik.http.routers.nginx.tls.certresolver=le',
            'traefik.http.services.nginx.loadbalancer.server.port=80',
            'traefik.http.routers.nginx.rule=Host(`cubieserver.de`) || Host(`www.cubieserver.de`) || Host(`blog.cubieserver.de`) || Host(`adele.cubieserver.de`) || Host(`adeles.cooking`)',
            'traefik.http.routers.nginx.middlewares=security-headers@file',
          ],
          'resources' => {
            'limits' => {
              'memory' => '128M',
              'cpus'   => '2.0',
            },
            'reservations' => {
              'memory' => '32M',
            }
          }
        }
      }
    }
  }

  profile::to_compose { 'nginx':
    compose => $compose,
  }

}
