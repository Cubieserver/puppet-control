class profile::swarm::nextcloud (
  String $admin_password,
  String $db_password,
  String $exporter_token,
  String $exporter_version = 'latest',
  String $version = 'latest',
) {

  file { ['/mnt/data/nextcloud', '/mnt/data/nextcloud/config']:
    ensure => directory,
    owner  => 'www-data',
    group  => 'www-data',
  }

  ::mysql::db { 'nextcloud':
    user     => 'nextcloud',
    password => $db_password,
    host     => 'localhost',
    grant    => [ 'SELECT', 'UPDATE', 'CREATE', 'INSERT', 'ALTER', 'DELETE', 'DROP', 'LOCK TABLES', 'INDEX' ],
  }

  file { '/mnt/data/nextcloud/config/autoconfig.php':
    ensure  => present,
    content => template('profile/nextcloud_autoconfig.php.erb'),
    require => File['/mnt/data/nextcloud/config'],
  }

  $compose = {
    'version' => '3.7',
    'services' => {
      'nextcloud' => {
        'image'           => "nextcloud:${version}",
        'volumes'         => [
          '/var/run/mysqld/mysqld.sock:/var/run/mysqld/mysqld.sock',
          '/var/run/clamav/clamd.ctl:/var/run/clamav/clamd.ctl',
          '/mnt/data/nextcloud:/var/www/html',
        ],
        'deploy'          => {
          'labels'        => [
            'traefik.enable=true',
            'traefik.http.routers.nextcloud.tls.certresolver=le',
            'traefik.http.services.nextcloud.loadbalancer.server.port=80',
            'traefik.http.routers.nextcloud.rule=Host(`cloud.cubieserver.de`)',
            'traefik.http.routers.nextcloud.middlewares=security-headers@file',
          ],
          'resources' => {
            'limits' => {
              'memory' => '2048M',
              'cpus'   => '4.0',
            },
            'reservations' => {
              'memory' => '512M',
            }
          }
        },
      },
      'nextcloud-cron' => {
        'image'      => "nextcloud:${version}",
        'volumes'    => [
          '/var/run/mysqld/mysqld.sock:/var/run/mysqld/mysqld.sock',
          '/mnt/data/nextcloud:/var/www/html',
        ],
        'entrypoint' => '/cron.sh',
        'deploy' => {
          'resources' => {
            'limits' => {
              'memory' => '128M',
              'cpus'   => '0.25',
            },
            'reservations' => {
              'memory' => '64M',
            }
          }
        }
      },
      'nextcloud-exporter' => {
        'image'       => "xperimental/nextcloud-exporter:${exporter_version}",
        'read_only'   => true,
        'environment' => [
          'NEXTCLOUD_SERVER=http://nextcloud',
          'NEXTCLOUD_USERNAME=admin',
          "NEXTCLOUD_PASSWORD=${exporter_token}",
        ],
        'deploy'      => {
          'resources' => {
            'limits' => {
              'memory' => '64M',
              'cpus'   => '0.25',
            },
            'reservations' => {
              'memory' => '32M',
            }
          }
        }
      }
    }
  }

  profile::to_compose { 'nextcloud':
    compose => $compose,
  }

}
