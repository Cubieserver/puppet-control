class profile::dns (
  Array[String] $primary_dns_servers,
) {

  file { '/etc/resolv.conf':
    ensure  => link,
    target  => '/run/systemd/resolve/stub-resolv.conf',
    replace => true,
  }

  file { '/etc/systemd/resolved.conf':
    ensure  => present,
    content => template('profile/systemd-resolved.conf.erb'),
    notify  => Service['systemd-resolved'],
  }

  service { 'systemd-resolved':
    ensure   => running,
    enable   => true,
    provider => 'systemd',
  }

  # Note: no additional firewall rules necessary, because traffic on loopback interface is always allowed

}
