class profile::intel_backlight (

) {

  file { '/usr/local/bin/intel_backlight.sh':
    ensure  => present,
    mode    => '0755',
    content => template('profile/intel_backlight.sh.erb'),
  }

  file { '/etc/udev/rules.d/90-intel_backlight.rules':
    ensure  => present,
    content => template('profile/90-intel_backlight.rules'),
  }

}
