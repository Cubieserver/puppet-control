class profile::firewall_post (
  Boolean $debug_drop = $profile::firewall::debug_drop,
){
  if $debug_drop {
    firewall { '998 DEBUG DROP':
      proto      => 'all',
      jump       => 'LOG',
      limit      => '5/min',
      log_prefix => 'DROPPED INPUT: ',
      log_level  => '7',
      before     => undef,
    }

    firewall { '998 DEBUG DROP FORWARD':
      chain      => 'FORWARD',
      proto      => 'all',
      jump       => 'LOG',
      limit      => '5/min',
      log_prefix => 'DROPPED FORWARD: ',
      log_level  => '7',
      before     => undef,
    }
  }

  # catch-all rule to drop the rest
  firewall { '999 drop all':
    proto  => 'all',
    action => 'drop',
    before => undef,
  }
  firewall { '999 drop all (v6)':
    proto    => 'all',
    action   => 'drop',
    before   => undef,
    provider => 'ip6tables',
  }
}
