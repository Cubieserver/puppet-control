class profile::tor_relay (
  Optional[String] $nickname = undef,
  Optional[String] $contactinfo = undef,
  Optional[String] $log_level = 'notice',
  Integer $orport = 9001,
  Array[String] $exitpolicy = ['reject *:*'],
) {

  file { '/etc/tor/torrc':
    ensure  => present,
    content => template('profile/torrc.erb'),
    owner   => 'debian-tor',
    group   => 'debian-tor',
    mode    => '0600',
    notify  => Service['tor.service'],
    require => Package['tor'],
  }

  # install tor package from backports
  # no-install-recommends avoids installing exim4
  package { ['tor', 'tor-geoipdb', 'torsocks']:
    ensure          => present,
    install_options => ['-t', "${::lsbdistcodename}-backports", '--no-install-recommends'],
  }

  service { 'tor.service':
    ensure   => stopped,
    provider => 'systemd',
  }

  firewall { '100 allow tor relay traffic':
    dport  => $orport,
    proto  => tcp,
    action => accept,
  }

}
