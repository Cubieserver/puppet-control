class profile::swarm (

) {

  contain ::profile::swarm::nextcloud
  contain ::profile::swarm::gitea
  contain ::profile::swarm::traefik
  contain ::profile::swarm::ejabberd
  contain ::profile::swarm::nginx
  contain ::profile::swarm::php_fpm
  contain ::profile::swarm::redis
  contain ::profile::swarm::minio
  contain ::profile::swarm::drone
  contain ::profile::swarm::conversejs
  contain ::profile::swarm::cockroach
  contain ::profile::swarm::u9k
  contain ::profile::swarm::swarmprom
  contain ::profile::swarm::grafana
  contain ::profile::swarm::node_exporter

  $compose = [
              '/mnt/data/swarm/traefik.yaml',
              '/mnt/data/swarm/nextcloud.yaml',
              '/mnt/data/swarm/gitea.yaml',
              '/mnt/data/swarm/ejabberd.yaml',
              '/mnt/data/swarm/nginx.yaml',
              '/mnt/data/swarm/php-fpm.yaml',
              '/mnt/data/swarm/redis.yaml',
              '/mnt/data/swarm/minio.yaml',
              '/mnt/data/swarm/drone.yaml',
              '/mnt/data/swarm/conversejs.yaml',
              '/mnt/data/swarm/cockroach.yaml',
              '/mnt/data/swarm/u9k.yaml',
              '/mnt/data/swarm/swarmprom.yaml',
              '/mnt/data/swarm/grafana.yaml',
              '/mnt/data/swarm/node-exporter.yaml',
              ]

  docker_stack { 'stack':
    ensure        => present,
    compose_files => $compose,
    up_args       => '--prune',
    require       => File[$compose],
    subscribe     => File[$compose],
  }


}
