class profile::autologin (
  String $user = 'jack',
  String $tty = 'tty1', # leave empty to log in on all ttys
) {

  $unit = "/etc/systemd/system/getty@${tty}.service"

  file { "${unit}.d":
    ensure => directory,
  }

  # template requires $user;
  file { "${unit}.d/override.conf":
    ensure  => present,
    content => template('profile/getty-override.conf'),
    require => File["${unit}.d"],
  }

}


