
class profile::openldap (
  String $admin_password,
  String $ssl_key_path,
  String $ssl_cert_path,
  String $server_name = 'example',
  String $tld = 'com',
) {

  $domain = "dc=${server_name},dc=${tld}"

  # allow openldap to access ssl-certificates
  user { 'openldap':
    groups  => ['ssl-cert'],
    require => [ Package['ssl-cert'], Package['slapd'] ],
    notify  => Service['slapd'],
  }

  class { '::openldap::client':
    base => $domain,
  }

  class { '::openldap::server':
    provider  => 'olc',
    ssl_key   => $ssl_key_path,
    ssl_cert  => $ssl_cert_path,
    ldap_ifs  => ['/'], # listen on port 389
    ldaps_ifs => ['/'], # listen on port 636
    ldapi_ifs => ['/'], # listen on unix socket /var/run/slapd/ldapi
  }

  openldap::server::database { $domain:
    ensure => present,
    rootdn => "cn=admin,${domain}",
    rootpw => $admin_password,
  }

  # load and install additional modules
  openldap::server::module { 'memberof':
    ensure => present,
  }

  openldap::server::overlay { "memberof on ${domain}":
    ensure => present,
  }

  # load additional schemas
  openldap::server::schema { 'inetorgperson':
    ensure => present,
  }

  firewall { '101 allow secure ldap traffic':
    dport  => '636',
    proto  => tcp,
    action => accept,
  }

  firewall { '101 allow secure ldap traffic (v6)':
    dport    => '636',
    proto    => tcp,
    action   => accept,
    provider => 'ip6tables',
  }

  # Access Control

  # only admin can write userPassword and shadowLastChange, everyone else (including bind) cannot read or write
  openldap::server::access { "{0}to attrs=userPassword,shadowLastChange by dn=\"cn=admin,${domain}\" write by anonymous auth by * none on ${domain}" : }

  # user (self) can read all attributes, admin can write all attributes, bind can read all attributes, everyone else can do anything
  openldap::server::access { "{1}to * by self read by dn=\"cn=admin,${domain}\" write by dn=\"cn=bind,${domain}\" read by * none on ${domain}" : }

  # template for backup job also uses $domain
  file { '/usr/local/sbin/openldap-backup.sh':
    ensure  => present,
    mode    => '0751',
    content => template('profile/openldap-backup.sh.erb'),
  }

  # setup backup job
  cron { 'backup openldap database':
    command => '/usr/local/sbin/openldap-backup.sh',
    hour    => 22,
    minute  => 22,
    require => File['/usr/local/sbin/openldap-backup.sh'],
  }

  # TODO: schemas required?
  #openldap::server::schema { 'samba':
    #  ensure  => present,
    #  require => Openldap::Server::Schema['inetorgperson'],
    #}

  #openldap::server::schema { 'nis':
    #  ensure  => present,
    #  require => Openldap::Server::Schema['inetorgperson'],
    #}

  # TODO: password policy module ppolicy required?

  # install additional database utilities
  package { 'db-util':
    ensure => present,
  }

}
