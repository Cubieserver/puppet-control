class profile::k3s (
  String $version = 'stable',
  Array[String] $node_labels = [],
  ) {

  exec { 'download k3s installer':
    command => '/usr/bin/curl -o /opt/k3s-install.sh https://get.k3s.io',
    creates => '/opt/k3s-install.sh',
    timeout => 10,
  }

  file { '/etc/k3s-config.yaml':
    ensure  => present,
    content => template('profile/k3s-config.yaml.erb'),
  }

  exec { 'ensure k3s is installed':
    path        => '/usr/local/bin:/usr/bin:/bin',
    provider    => shell,
    command     => 'bash /opt/k3s-install.sh -c /etc/k3s-config.yaml --no-deploy traefik',
    environment => ["INSTALL_K3S_VERSION=${version}"],
    unless      => "k3s --version | grep 'k3s version ${version}'",
    timeout     => 60,
  }

  firewall { '100 allow kubernetes control plane traffic from external':
    dport  => 6443,
    proto  => tcp,
    action => accept,
  }

  # create cluster definitions directory
  file { '/etc/k8s-applies/':
    ensure => directory,
    mode   => '0750',
  }

  # setup jacks namespace
  file { '/etc/k8s-applies/jack.yaml':
    ensure  => present,
    content => template('profile/k8s/jack.yaml'),
    require => File['/etc/k8s-applies'],
  }

  exec { 'apply cluster definitions':
    command => '/usr/local/bin/k3s kubectl apply -R -f /etc/k8s-applies/ --wait',
    require => File['/etc/k8s-applies'],
  }

}
