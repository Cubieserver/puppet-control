class profile::efiboot (

) {

  # see https://wiki.debian.org/EFIStub

  $copy_vmlinuz_template = @(END)
  #!/bin/sh
  # This file is managed by Puppet
  cp /vmlinuz /boot/efi/EFI/debian/
  END

  file { '/etc/kernel/postinst.d/zz-update-efistub':
    ensure  => present,
    mode    => '0751',
    content => inline_template($copy_vmlinuz_template),
  }

  $copy_initrd_template = @(END)
  #!/bin/sh
  # This file is managed by Puppet
  cp /initrd.img /boot/efi/EFI/debian/
  END

  file { '/etc/initramfs-tools/hooks/zz-update-efistub':
    ensure  => present,
    mode    => '0751',
    content => inline_template($copy_initrd_template),
  }

}
