class profile::unattended_upgrades (
  Optional[String] $mail_notification_to = undef,
  ) {

  package { 'unattended-upgrades':
    ensure => present,
  }

  file { '/etc/apt/apt.conf.d/20auto-upgrades':
    ensure => present,
  }

  file_line { 'enable unattended-upgrades':
    path  => '/etc/apt/apt.conf.d/20auto-upgrades',
    line  => 'APT::Periodic::Unattended-Upgrade "1";',
    match => 'APT::Periodic::Unattended\-Upgrade',
  }

  file_line { 'enable automatic list updates':
    path  => '/etc/apt/apt.conf.d/20auto-upgrades',
    line  => 'APT::Periodic::Update-Package-Lists "1";',
    match => 'APT::Periodic::Update\-Package\-Lists',
  }

  if $mail_notification_to {
    file_line { 'enable email notifications':
      path  => '/etc/apt/apt.conf.d/50unattended-upgrades',
      line  => "Unattended-Upgrade::Mail \"${mail_notification_to}\";",
      match => 'Unattended\-Upgrade::Mail .*;$',
    }
  } else {
    file_line { 'disable email notifications':
      ensure            => absent,
      path              => '/etc/apt/apt.conf.d/50unattended-upgrades',
      match             => '^Unattended\-Upgrade::Mail .*;$',
      match_for_absence => true,
    }
  }
  # unattended-upgrades defaults in /etc/apt/apt.conf.d/50unattended-upgrades are fine, leaving them as they are

}
