class profile::mariadb (

  String $root_password,
  String $backup_password,
  String $backup_dir,

) {

  $default_override_options = {
    # enable 4-byte support for mariadb 10.1
    'mariadb' => {
      innodb_file_per_table => '1',
      innodb_file_format    => 'barracuda',
      innodb_large_prefix   => 'on',
    }
  }

  class { '::mysql::server':
    root_password           => $root_password,
    package_name            => 'mariadb-server',
    remove_default_accounts => true,
    restart                 => true,
    service_manage          => true,
    service_enabled         => true,
    override_options        => deep_merge(
      $default_override_options,
      {},
    ),
  }

  class { '::mysql::client':
    package_name => 'mariadb-client',
  }

  class { '::mysql::server::backup':
    backupuser        => 'backup',
    backuppassword    => $backup_password,
    backupdir         => $backup_dir,
    backupdirmode     => '0700',
    backupdirowner    => root,
    backupdirgroup    => root,
    file_per_database => true,
    time              => ['01', '30'],
    maxallowedpacket  => '16M',
  }
}
