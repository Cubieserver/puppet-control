class profile::turn (

  String $secret,
  String $domain,
  Integer $port = 3478,

) {

  package { 'coturn':
    ensure          => present,
    install_options => ['-t', "${::lsbdistcodename}-backports"],
  }

  # configured according to:
  # https://help.nextcloud.com/t/howto-setup-nextcloud-talk-with-turn-server/30794
  file { '/etc/turnserver.conf':
    ensure  => present,
    content => template('profile/turnserver.conf.erb'),
    require => Package['coturn'],
  }

  service { 'coturn':
    ensure  => running,
    require => Package['coturn'],
  }

  firewall { '100 allow coTURN TCP traffic':
    dport  => $port,
    proto  => tcp,
    action => accept,
  }

  firewall { '100 allow coTURN TCP traffic (v6)':
    dport    => $port,
    proto    => tcp,
    action   => accept,
    provider => 'ip6tables',
  }

  firewall { '100 allow coTURN UDP traffic':
    dport  => $port,
    proto  => udp,
    action => accept,
  }

  firewall { '100 allow coTURN UDP traffic (v6)':
    dport    => $port,
    proto    => udp,
    action   => accept,
    provider => 'ip6tables',
  }

}
