class profile::ssh_client (
  Hash $authorized_keys,
) {

  $default = {
    'ensure' => absent,
    'user'   => root,
  }

  create_resources('ssh_authorized_key', $authorized_keys, $default)

}
