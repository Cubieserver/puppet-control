class profile::firewall (
  Array[String] $ignore_purge,
  String $forward_policy = 'drop',
  Boolean $debug_drop = false,
) {

  Firewall {
    before  => Class['profile::firewall_post'],
    require => Class['profile::firewall_pre'],
  }

  contain profile::firewall_pre
  contain profile::firewall_post

  contain ::firewall

}
