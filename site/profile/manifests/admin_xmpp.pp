class profile::admin_xmpp (
  String $xmta_server_url,
  String $xmta_token,
) {

  file { '/usr/local/sbin/admin-xmpp.sh':
    ensure  => present,
    mode    => '0700',
    owner   => 'root',
    content => template('profile/admin-xmpp.sh.erb'),
  }

}
