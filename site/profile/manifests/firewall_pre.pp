class profile::firewall_pre (
  Array[String] $ignore_purge = $profile::firewall::ignore_purge,
){
  Firewall {
    require => undef,
  }

  # set INPUT, FORWARD and NAT chains to DROP, purge existing config except the ignored ones
  firewallchain { ['INPUT:filter:IPv4', 'INPUT:filter:IPv6']:
    ensure => present,
    purge  => true,
    policy => drop,
    ignore => $ignore_purge,
  }

  firewallchain { ['FORWARD:filter:IPv4', 'FORWARD:filter:IPv6']:
    ensure => present,
    purge  => true,
    policy => drop,
    ignore => $ignore_purge,
  }
  firewallchain { ['PREROUTING:nat:IPv4', 'PREROUTING:nat:IPv6']:
    ensure => present,
    purge  => true,
    ignore => $ignore_purge,
  }
  firewallchain { ['POSTROUTING:nat:IPv4', 'POSTROUTING:nat:IPv6']:
    ensure => present,
    purge  => true,
    ignore => $ignore_purge,
  }

  # allow all outgoing communication by defaut (ACCEPT)
  firewallchain { ['OUTPUT:filter:IPv4', 'OUTPUT:filter:IPv6']:
    ensure => present,
    purge  => true,
    policy => accept,
    ignore => $ignore_purge,
  }

  # sane default firewall rules
  firewall { '000 accept all icmp':
    proto  => 'icmp',
    action => 'accept',
  }->
  firewall { '000 accept all icmp (v6)':
    proto    => 'ipv6-icmp',
    action   => 'accept',
    provider => 'ip6tables',
  }->
  firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }->
  firewall { '001 accept all to lo interface (v6)':
    proto    => 'all',
    iniface  => 'lo',
    action   => 'accept',
    provider => 'ip6tables',
  }->
  firewall { '002 reject local traffic not on loopback interface':
    iniface     => '! lo',
    proto       => 'all',
    destination => '127.0.0.1/8',
    action      => 'reject',
  }->
  firewall { '002 reject local traffic not on loopback interface (v6)':
    iniface     => '! lo',
    proto       => 'all',
    destination => '::1/8',
    action      => 'reject',
    provider    => 'ip6tables',
  }->
  firewall { '003 accept related established rules':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }->
  firewall { '003 accept related established rules (v6)':
    proto    => 'all',
    state    => ['RELATED', 'ESTABLISHED'],
    action   => 'accept',
    provider => 'ip6tables',
  }->
  firewall { '004 accept all SSH':
    dport  => [22, 50022],
    proto  => tcp,
    action => accept,
  }->
  firewall { '004 accept all SSH (v6)':
    dport    => [22, 50022],
    proto    => tcp,
    action   => accept,
    provider => 'ip6tables',
  }
}
