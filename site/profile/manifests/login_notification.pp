class profile::login_notification(

) {
  file { '/usr/local/sbin/login-notification.sh':
    ensure  => present,
    mode    => '0700',
    owner   => 'root',
    content => template('profile/login-notification.sh.erb'),
  }

  file_line { 'Add login-notification hook to PAM':
    path  => '/etc/pam.d/common-session',
    line  => 'session optional       pam_exec.so /usr/local/sbin/login-notification.sh',
    match => 'login-notification.sh',
  }
}
