class profile::docker (
  String $version = 'latest',
  String $log_level = 'info',
  String $ipv6_cird = 'fde8:1ac4::/48',
  Optional[Array] $dns = undef,
) {

  # see https://github.com/puppetlabs/puppetlabs-docker/blob/master/manifests/init.pp for all options
  class { '::docker':
    version          => $version,
    log_level        => $log_level,
    dns              => $dns,
    # ipv6_cidr        => $ipv6_cidr,
    # ipv6             => true,
    # enable metrics for prometheus
    extra_parameters => ['--experimental=true', '--metrics-addr=0.0.0.0:9323'],
  }

  firewall { '100 allow internal access to docker metrics':
    destination => '172.18.0.1',
    dport       => '9323',
    proto       => tcp,
    action      => accept,
  }

}
