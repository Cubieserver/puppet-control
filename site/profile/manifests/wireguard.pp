class profile::wireguard (
  String $subnet,
  String $ip,
  String $interface = 'wg0', # default wireguard interface name
  Integer $internal_port = 51820, # default wireguard port
  Integer $external_port = 51820,
  ) {

  package { ['wireguard', 'wireguard-dkms', 'resolvconf']:
    ensure          => present,
    install_options => ['-t', "${::lsbdistcodename}-backports"],
  }

  file { '/etc/wireguard':
    ensure => directory,
  }

  # firewall rules
  firewall { '100 allow incoming wireguard VPN traffic':
    dport  => $external_port,
    proto  => udp,
    action => accept,
  }

  firewall { '100 allow incoming wireguard VPN traffic (v6)':
    dport    => $external_port,
    proto    => udp,
    action   => accept,
    provider => 'ip6tables',
  }

  firewall { '100 allow VPN connections inbound':
    chain    => 'FORWARD',
    proto    => 'all',
    outiface => $interface,
    state    => ['RELATED', 'ESTABLISHED'],
    action   => accept,
  }

  firewall { '100 allow VPN connections outbound':
    chain   => 'FORWARD',
    proto   => 'all',
    iniface => $interface,
    action  => accept,
  }

  firewall { '100 NAT VPN traffic':
    table  => 'nat',
    chain  => 'POSTROUTING',
    proto  => 'all',
    source => $subnet,
    jump   => 'MASQUERADE',
  }

  if $external_port != $internal_port {
    firewall { '100 Redirect VPN external port to internal port':
      table   => 'nat',
      chain   => 'PREROUTING',
      iniface => 'enp3s0',
      proto   => 'udp',
      dport   => $external_port,
      toports => $internal_port,
      jump    => 'REDIRECT',
    }

    firewall { '100 Redirect VPN external port to internal port (v6)':
      table    => 'nat',
      chain    => 'PREROUTING',
      iniface  => 'enp3s0',
      proto    => 'udp',
      dport    => $external_port,
      toports  => $internal_port,
      jump     => 'REDIRECT',
      provider => 'ip6tables',
    }
  }

  # DNS setup
  firewall { '100 allow DNS queries on VPN interface':
    proto       => 'udp',
    iniface     => $interface,
    dport       => '53',
    source      => $subnet,
    destination => "${ip}/32",
    action      => accept,
  }

  # relay VPN DNS traffic to systemd-resolved
  $bind_ip = $ip
  # $after = "wg-quick@${interface}.service"
  $after = "sys-devices-virtual-net-${interface}.device"
  file { '/etc/systemd/system/vpn-dns-relay.service':
    ensure  => present,
    content => template('profile/dns-relay.service.erb'),
    notify  => Exec['systemd daemon-reload for vpn-dns-relay.service'],
  }

  exec { 'systemd daemon-reload for vpn-dns-relay.service':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
  }

  service { 'vpn-dns-relay.service':
    ensure   => running,
    enable   => true,
    require  => File['/etc/systemd/system/vpn-dns-relay.service'],
    provider => 'systemd',
  }

}
