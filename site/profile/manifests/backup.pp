class profile::backup (
  String $restic_repo,
  String $restic_pass,
  Hash $environment,
  Array[String] $exclude_paths,
  Array[String] $backup_paths,
  String $metrics_file = '/var/lib/node-exporter/restic-backup.prom',
  Optional[String] $time = undef,
  Optional[String] $mail_notification = undef,
) {

  # Ensure restic is installed
  package { 'restic':
    ensure => present,
  }

  # Install systemd service unit for backup job
  file { '/etc/systemd/system/restic-backup.service':
    ensure  => present,
    content => template('profile/restic-backup.service.erb'),
    # notify  => Service['restic-backup'],
    require => [
      File['/etc/systemd/system/restic-backup-failure.service'],
      File['/usr/local/sbin/restic-backup.sh'],
      File['/usr/local/sbin/restic-prune.sh'],
      File['/usr/local/sbin/restic-exporter.sh'],
      File['/usr/local/bin/cleanup-backups.sh'],
    ],
  }

  # Service unit for publishing errors
  file { '/etc/systemd/system/restic-backup-failure.service':
    ensure  => present,
    content => template('profile/restic-backup-failure.service.erb'),
    require => [
      File['/usr/local/sbin/restic-exporter.sh'],
    ]
  }

  if $time != undef {
    # Timer (aka cron job)
    file { '/etc/systemd/system/restic-backup.timer':
      ensure  => present,
      content => template('profile/restic-backup.timer.erb'),
      notify  => Service['restic-backup'],
    }

    # Enable timer
    # TODO: systemctl daemon-reload when service files change
    service { 'restic-backup':
      ensure   => running,
      enable   => true,
      provider => 'systemd',
      name     => 'restic-backup.timer',
      require  => [
        File['/etc/systemd/system/restic-backup.service'],
        File['/etc/systemd/system/restic-backup.timer'],
      ],
    }
  }

  # config (secrets) in environment variables
  $env_file = '/etc/restic-env.sh'
  file { $env_file:
    ensure  => present,
    content => template('profile/restic-env.sh.erb'),
    mode    => '0600',
  }

  # script to purge old backups
  file { '/usr/local/bin/cleanup-backups.sh':
    ensure  => present,
    content => template('profile/cleanup-backups.sh'),
    mode    => '0751',
  }

  # repo cleanup script
  file { '/usr/local/sbin/restic-prune.sh':
    ensure  => present,
    content => template('profile/restic-prune.sh.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0751',
    require => Package['restic'],
  }

  # main backup script
  file { '/usr/local/sbin/restic-backup.sh':
    ensure  => present,
    content => template('profile/restic-backup.sh.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0751',
    require => Package['restic'],
  }

  # prometheus exporter script
  file { '/usr/local/sbin/restic-exporter.sh':
    ensure  => present,
    content => template('profile/restic-exporter.sh.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0751',
  }

}
