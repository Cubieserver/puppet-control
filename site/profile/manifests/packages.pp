class profile::packages (
  Hash $packages,
) {

  $package_default = {
    ensure   => present,
    provider => 'apt',
  }

  ensure_packages($packages, $package_default)

}
