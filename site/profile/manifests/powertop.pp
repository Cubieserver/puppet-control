class profile::powertop (

) {

  package { 'powertop':
    ensure => present,
  }

  file { '/etc/systemd/system/powertop.service':
    ensure  => present,
    content => template('profile/powertop.service'),
  }~>
  exec { 'reload systemctl for powertop':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
  }~>
  service { 'powertop.service':
    ensure  => running,
    require => Package['powertop']
  }

}
