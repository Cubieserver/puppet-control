# from vision_docker: https://github.com/vision-it/vision-docker
define profile::to_compose (
  Hash $compose,
  String $path = '/mnt/data/swarm',
  String $owner = 'root',
  String $group = 'root',
  String $mode = '0600',

  ) {

  if !defined(File[$path]) {
    file { $path:
      ensure => directory,
    }
  }

  file { "${path}/${title}.yaml":
    ensure  => present,
    owner   => $owner,
    group   => $group,
    mode    => $mode,
    content => inline_template("# This file is managed by Puppet\n<%= @compose.to_yaml %>"),
  }

}
