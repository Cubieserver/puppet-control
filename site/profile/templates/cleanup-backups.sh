#!/bin/bash
set -e

echo 'Starting cleaning up backup files'

# delete backups older than 7 days
find /var/backups/ -mtime +7 -print -delete

echo 'Finished cleaning up backup files'
