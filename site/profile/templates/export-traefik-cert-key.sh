#!/bin/sh

set -e

if [ -z "$1" ]; then
    echo "No domain given"
else
    DOMAIN="$1"
fi

if ! grep \"${DOMAIN}\" /etc/traefik/acme.json > /dev/null; then
    echo "Domain $DOMAIN not found in /etc/traefik/acme.json"
    exit 1
fi

# extract certificate
cat /etc/traefik/acme.json | jq -r '.Certificates[] | select(.Domain.Main=="'${DOMAIN}'") | .Certificate' | base64 -d > /etc/ssl/private/${DOMAIN}.crt

# extract private key
cat /etc/traefik/acme.json | jq -r '.Certificates[] | select(.Domain.Main=="'${DOMAIN}'") | .Key' | base64 -d > /etc/ssl/private/${DOMAIN}.key

# ensure strict permissions are applied
chown root:ssl-cert /etc/ssl/private/${DOMAIN}.{key,crt}
chmod 640 /etc/ssl/private/${DOMAIN}.{key,crt}
