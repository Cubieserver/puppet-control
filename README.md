# Puppet Control Repo

Put this repository into `~/.puppet/`.

## Install Puppet Modules

```
puppet module install --target 'modules'  'puppetlabs-apt' --version '6.2.1'
```

Upgrade with:
```
puppet module upgrade --modulepath 'modules' --version=10.3.0 'puppetlabs-mysql'
```

Commit modules to Git!
