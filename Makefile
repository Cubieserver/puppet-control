#!/usr/bin/make

PHONY: clean provision check validate yamllint puppetlint

# executes a puppet run to provision the machine
provision: check
	puppet apply --config etc/puppet.conf manifests/site.pp

# runs all checks on code and config
check: puppetlint validate yamllint validatetemplates

# checks that puppet manifests conform to the style guide
puppetlint:
	puppet-lint --no-documentation-check --no-parameter_order-check manifests/
	puppet-lint --no-documentation-check site/

# checks puppet syntax
validate:
	puppet parser validate manifests/site.pp site/profile/manifests/

# checks yaml syntax
yamllint:
	yamllint etc/ site/profile/

# check template syntax
validatetemplates:
	for i in site/profile/templates/*.erb ; do \
		erb -P -x -T '-' $$i | ruby -c ; \
	done

# deletes caches and runtime directories
clean:
	rm -rf cache/ var/
