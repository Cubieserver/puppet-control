node default {
  $description = lookup('description')
  notice("${::fqdn}: ${description}")
  contain profile::example
}

node '02.ht.cubieserver.de' {
  $description = lookup('description')
  notice("${::fqdn}: ${description}")

  # set up ssh authorized keys
  contain profile::ssh_client

  # set up repositories
  contain profile::repos

  # install packages
  contain profile::packages

  # install and set up unattended-upgrades
  contain profile::unattended_upgrades

  # set up backup job
  contain profile::backup

  # set up docker daemon
  contain profile::docker

  # set up tor relay
  contain profile::tor_relay

  # configure firewall
  contain profile::firewall

  # configure antivirus with clamav
  contain profile::antivirus

  # set up openldap
  contain profile::openldap

  # set up mysql server
  contain profile::mariadb

  # run TURN server
  contain profile::turn

  # set up local DNS server
  contain profile::dns

  # deploy docker swarm stack
  contain profile::swarm

  # install script for admin notifications via XMPP
  contain profile::admin_xmpp

  # set up notifications upon user login
  contain profile::login_notification

  # setup k3s cluster
  contain profile::k3s

  # # wireguard VPN server (without config, just setup)
  # contain profile::wireguard
}

node 'ZenBook' {
  $description = lookup('description')
  notice("${::fqdn}: ${description}")

  user { 'jack':
    ensure => present,
    groups => ['sudo', 'audio', 'video', 'plugdev', 'netdev', 'bluetooth'],
    shell  => '/bin/bash',
  }

  # let user configure display backlight
  contain profile::intel_backlight

  # install default packages
  contain profile::packages

  # auto login
  contain profile::autologin

  # set power management tunables
  contain profile::powertop

  # restic backups
  contain profile::backup

  # set up efistub booting
  contain profile::efiboot

}
